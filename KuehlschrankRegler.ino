#include "oled.h"
#include <Arduino.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include "EEPROMAnything.h"
#include <avr/wdt.h> //should be in any adruino IDE
#include <limits.h>
#include <stdint.h>


#ifdef QT_CREATOR
#include <pgmspace.h>
#endif

/*  OLED          arduino
	 D0-----------10
	 D1-----------9
	 RST----------13
	 DC-----------11
	 VCC----------5V
	 GND----------GND*/
// see oled.cpp

/*
 * TODOs:
 * set unused pins to outputs
 * handle automatic reset
 */


//
const int pinHotter = A3;
const int pinCooler = A4;
const int pinTemperature = A5;
const int pinRelay = 3;

const uint32_t MagicInFlash = 0x12345678;
const int32_t MinOffTime = 120000; // two minutes
	// compressor must have some time to exand the gas, until it can restart


float T=-30; // sensor temperature in °C
float Tref = 6.0; // reference temperature in °C
const float hyst = 1.0; // symmetric hysteresis in °C
bool turnedOn = false;
int64_t lastTimeOff = 0; // in ms

static bool lastBC = false;
static bool lastBH = false;
static int64_t bcLastReleased = 0;
static int64_t bcLastPressed = 0;
static int64_t bhLastReleased = 0;
static int64_t bhLastPressed = 0;

static int64_t millisRequestTemperature = 0;
static bool waitingForTemperature = false;

static int64_t lastSaved = 0;


// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(pinTemperature);

// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

// 64-bit millis-function
int64_t millis64(bool reset=false)
{
	static int64_t millis64=0;
	static int64_t millisOld=0;

	int64_t millisNow = millis();

	if(reset){
		millis64 = 0;
	}else{
		if(millisNow < millisOld){
			millis64 += (int64_t)UINT32_MAX-millisOld;
			millis64 += millis();
		}else{
			millis64 += millisNow - millisOld;
		}
	}
	millisOld = millisNow;
}

void turnOn()
{
	digitalWrite(pinRelay, HIGH);
}

void turnOff()
{
	digitalWrite(pinRelay, LOW);
}

void setup()
{
	wdt_disable(); //always good to disable it, if it was left 'on' or you need init time

	LED_Init();

	sensors.begin();
	sensors.setWaitForConversion(false);  // makes it async

	pinMode(pinRelay, OUTPUT);
	turnOff();

	digitalWrite(pinCooler, HIGH); // set Pullup
	digitalWrite(pinHotter, HIGH); // set Pullup

	uint32_t magicInEEPROM;
	EEPROM_readAnything(0, magicInEEPROM);
	if(magicInEEPROM == MagicInFlash){
		EEPROM_readAnything(4, Tref);
	}else{
		EEPROM_writeAnything(0, MagicInFlash);
		EEPROM_writeAnything(4, Tref);
	}

	wdt_enable(WDTO_4S); //enable it, and set it to 8s


	// reset all variables
	turnOff();
	turnedOn = false;
	lastTimeOff = 0; // in ms

	lastBC = false;
	lastBH = false;
	bcLastReleased = 0;
	bcLastPressed = 0;
	bhLastReleased = 0;
	bhLastPressed = 0;

	millisRequestTemperature = 0;
	waitingForTemperature = false;

	lastSaved = 0;
	millis64(true); // reset this counter
}



void controller()
{
	if(!turnedOn && (T >= (Tref + hyst/2)) &&
			(millis64() > (lastTimeOff + MinOffTime) )){
		turnOn();
		turnedOn = true;
	}

	if(turnedOn && (T <= (Tref - hyst/2))){
		turnOff();
		lastTimeOff = millis64();
		turnedOn = false;
	}
}

void buttons()
{
	bool bC = !digitalRead(pinCooler);
	bool bH = !digitalRead(pinHotter);



	if(!lastBC && bC && (millis64() > (bcLastReleased+20))){
		Tref-=0.5;
		bcLastPressed = millis64();
		lastBC= true;
	}
	if(lastBC && !bC && (millis64() > (bcLastPressed+20))){
		bcLastReleased = millis64();
		lastBC = false;
	}

	if(!lastBH && bH && (millis64() > (bhLastReleased+20))){
		Tref+=0.5;
		bhLastPressed = millis64();
		lastBH = true;
	}
	if(lastBH && !bH && (millis64() > (bhLastPressed+20))){
		bhLastReleased = millis64();
		lastBH = false;
	}

	// reset, when both buttons pressed for 1s
	if(bH && bC
			&& (millis64() > (bhLastPressed+1000))
			&& (millis64() > (bcLastPressed+1000))){
		//while(1); // wait for watchdog
		setup();
	}
}

// update values in eeprom every 10s, if they have changed.
void saveParameters()
{

	if(millis64() > (lastSaved+10000)){
		EEPROM_updateAnything(4, Tref);
	}
}

void loop()
{

	if(!waitingForTemperature && (millis64() > (millisRequestTemperature + 2000))){
		sensors.setWaitForConversion(false);
		sensors.requestTemperatures();
		sensors.setWaitForConversion(true);
		millisRequestTemperature = millis64();
		waitingForTemperature = true;
	}

	if(waitingForTemperature && (millis64() > (millisRequestTemperature + 1000))){
		waitingForTemperature = false;
		T = sensors.getTempCByIndex(0);
	}

	LED_P8x16Str(20,2,(String("Tref: ")+String(Tref,1)+"  ").c_str());
	LED_P8x16Str(20,4,(String("T:    ")+String(T,1)+"  ").c_str());
	if(turnedOn){
		LED_P8x16Str(20,6,"ON      ");
	}else{
		int64_t timeSinceOff = (millis64()-lastTimeOff);
		if(timeSinceOff < MinOffTime)
			LED_P8x16Str(20,6,(String("OFF ")+String((MinOffTime-timeSinceOff)/1000) + "   ").c_str());
		else
			LED_P8x16Str(20,6,"OFF     ");
	}

	buttons();
	controller();
	saveParameters();

	wdt_reset();
}

