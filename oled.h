#ifndef OLED_H
#define OLED_H

// code from
// https://gist.github.com/tegila/2ddbbb0d9d24fbf2e05e
// http://www.smart-prototyping.com/OLED-0.96inch-12864-display-module-blue.html


/*  OLED          arduino
	 D0-----------10
	 D1-----------9
	 RST----------13
	 DC-----------11
	 VCC----------5V
	 GND----------GND*/
// see oled.cpp

//#define SMALL_FONT

extern int SCL_PIN;//D0
extern int SDA_PIN; //D1
extern int RST_PIN;//RST
extern int DC_PIN; //DC

void LEDPIN_Init(void);
void LED_Init(void);


void LED_CLS(void);
void LED_Set_Pos(unsigned char x,unsigned char y);//Set the coordinate
void LED_WrDat(unsigned char data);   //Write Data

#ifdef SMALL_FONT
void LED_P6x8Char(unsigned char x,unsigned char y,const char ch);
void LED_P6x8Str(unsigned char x,unsigned char y,const char ch[]);
void LED_PrintValueC(unsigned char x, unsigned char y,char data);
void LED_PrintValueI(unsigned char x, unsigned char y, int data);
void LED_PrintValueF(unsigned char x, unsigned char y, float data, unsigned char num);
#endif

void LED_P8x16Str(unsigned char x,unsigned char y,const char ch[]);

void LED_PrintBMP(unsigned char x0,unsigned char y0,unsigned char x1,unsigned char y1,unsigned char bmp[]);
void LED_Fill(unsigned char dat);
void LED_PrintEdge(void);
void LED_Cursor(unsigned char cursor_column, unsigned char cursor_row);
void LED_PrintLine(void);


#endif // OLED_H
